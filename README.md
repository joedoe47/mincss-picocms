MinCSS Theme
==================


## Milliseconds matter.

Amazon found that each 100ms delay cost them over a billion dollars in sales. Google found that 1/3 of this hit remained after decreasing loading times. Your CSS framework is costing you money.


## Tried and tested.

Min is used by over 65,000 people in 195+ countries, from North Korea to South Sudan to Mongolia to Somalia. Think your software is critical? Try a webapp keeping you alive in a warzone.


## Works everywhere.

That's no exaggeration. Min works on your decade-old Opera 9 install, your grandpa's IE5.5, and your kid's Nintendo DS. Wikipedia gets 500MM hits from IE6 a month... Bootstrap 4 doesn't even support IE8.


## Save time.

Min provides easy copy-and-paste examples for every element. You can get started with any of the provided templates, or you can read the complete and concise documentation with visual examples for every part of Min.

Min also styles your page using hundreds of times fewer properties vs. other CSS frameworks. This makes it easy to customize your page to look novel, avoiding the "bootstrapization" trend that makes your page look exactly like the rest.

# Side notes

- has both a dynamic and static method of generating menus (static is default because if statements in general run every time the client loads; less cpu cycles = good but more work when menu changes)

- can be used to generate a site or blog

- can easily use CDN version of MinCSS by uncommenting it

- to add a page create a folder called /page/ and add content 

- to add to blog create folder called /blog/ and add content to it

 
