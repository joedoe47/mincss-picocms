MinCSS Default Theme Changelog
============================

**Note:** This changelog only provides technical information about the changes
          introduced with a particular Pico version, and is meant to supplement
          the actual code changes. The information in this changelog are often
          insufficient to understand the implications of larger changes. Please
          refer to both the UPGRADE and NEWS sections of the docs for more
          details.

Initial Commit
======

Set up MinCSS theme with some basic features.
